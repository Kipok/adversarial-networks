import tensorflow as tf
import numpy as np
from datasets import BlurredDataset, ImagenetDataset
import matplotlib.pyplot as plt
import sys
from scipy.misc import imsave
import os

from operations import batch_norm, conv2d
from operations import fully_connected, lrelu


class AdversarialNetworks(object):
    def __init__(self, path_to_save, save_gpu_memory=False):
        # TODO: think of adding sess as a parameter
        super(AdversarialNetworks, self).__init__()
        self.sharp_gen = ImagenetDataset()
        self.blurred_gen = BlurredDataset()
        self.batch_size = 100
        self.img_size = 64
        self.path_to_save = path_to_save

        gpu_memory_fraction = 0.5 if save_gpu_memory else 0.9
        self.gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)

        self.blurred_imgs = tf.placeholder(
            'float',
            [self.batch_size, self.img_size, self.img_size, 1],
            name='blurred_imgs',
        )
        self.sharp_imgs = tf.placeholder(
            'float',
            [self.batch_size, self.img_size, self.img_size, 1],
            name='sharp_imgs',
        )

        g_out = self.generator(self.blurred_imgs, tf.constant(True, tf.bool))
        d_out = self.discriminator(self.sharp_imgs)
        gd_flow_out = self.discriminator(g_out, True)
        self.samples = self.generator(self.blurred_imgs, tf.constant(False, tf.bool)) / 2.0 + 0.5

        t_vars = tf.trainable_variables()
        d_vars = [var for var in t_vars if 'd_' in var.name]
        g_vars = [var for var in t_vars if 'g_' in var.name]

        eps = 1e-12
        self.d_cost = tf.reduce_mean(
            tf.log(1.0 - gd_flow_out + eps) + tf.log(d_out + eps),
            name='d_cost'
        )
        d_cost_sum = tf.scalar_summary('d_cost', self.d_cost)
        self.d_train_step = tf.train.AdamOptimizer(0.0002, 0.5).minimize(
            -self.d_cost, var_list=d_vars,
        )

        self.alpha = 1.0

        self.g_cost_reg = - self.alpha * tf.reduce_mean(tf.log(gd_flow_out + eps), name='g_cost')
        g_cost_reg_sum = tf.scalar_summary('g_cost_reg', self.g_cost_reg)
        self.g_cost_l2 = tf.reduce_sum(tf.pow(self.sharp_imgs - g_out, 2))
        g_cost_l2_sum = tf.scalar_summary('g_cost_l2', self.g_cost_l2)
        self.g_cost = self.g_cost_reg + self.g_cost_l2
        g_cost_sum = tf.scalar_summary('g_cost', self.g_cost)
        self.g_train_step = tf.train.AdamOptimizer(0.0002, 0.5).minimize(
            self.g_cost, var_list=g_vars,
        )

        self.real_accuracy = tf.reduce_mean(tf.cast(
            tf.equal(tf.round(d_out), 1),
            'float',
        ), name='real_accuracy')
        racc_sum = tf.scalar_summary('real_accuracy', self.real_accuracy)

        self.fake_accuracy = tf.reduce_mean(tf.cast(
            tf.equal(tf.round(gd_flow_out), 0),
            'float',
        ), name='fake_accuracy')
        facc_sum = tf.scalar_summary('fake_accuracy', self.fake_accuracy)

        self.merged_logs_d = tf.merge_summary([d_cost_sum, racc_sum, facc_sum])
        self.merged_logs_g = tf.merge_summary([g_cost_sum, g_cost_l2_sum, g_cost_reg_sum])

        self.saver = tf.train.Saver(max_to_keep=200)

    def discriminator(self, images, reuse=False):
        phase_train = tf.constant(True, tf.bool)
        if reuse:
            tf.get_variable_scope().reuse_variables()
            h0 = lrelu(conv2d(images, 64, name='d_h0'))
            h1 = lrelu(batch_norm(conv2d(h0, 128, name='d_h1'), phase_train, name='d_bn1'))
            h2 = lrelu(batch_norm(conv2d(h1, 256, name='d_h2'), phase_train, name='d_bn2'))
            h3 = lrelu(batch_norm(conv2d(h2, 512, name='d_h3'), phase_train, name='d_bn3'))
            out = tf.nn.sigmoid(fully_connected(h3, 1, name='d_out'))
            return out
        h0, self.dw0, self.db0 = conv2d(images, 64, name='d_h0', with_w=True)
        h0 = lrelu(h0)
        h1, self.dw1, self.db1 = conv2d(h0, 128, name='d_h1', with_w=True)
        h1 = lrelu(batch_norm(h1, phase_train, name='d_bn1'))
        h2, self.dw2, self.db2 = conv2d(h1, 256, name='d_h2', with_w=True)
        h2 = lrelu(batch_norm(h2, phase_train, name='d_bn2'))
        h3, self.dw3, self.db3 = conv2d(h2, 512, name='d_h3', with_w=True)
        h3 = lrelu(batch_norm(h3, phase_train, name='d_bn3'))
        out, self.dw4, self.db4 = fully_connected(h3, 1, name='d_out', with_w=True)
        return tf.nn.sigmoid(out)

    def generator(self, images, phase_train):
        h0, self.gw0, self.gb0 = conv2d(images, 32, f_h=30,
                                        f_w=1, s_h=1, s_w=1, name='g_h0', with_w=True)
        h0 = tf.nn.relu(batch_norm(h0, phase_train, name='g_bn0'))
        h1, self.gw1, self.gb1 = conv2d(h0, 32, f_h=1, f_w=30,
                                        s_h=1, s_w=1, name='g_h1', with_w=True)
        h1 = tf.nn.relu(batch_norm(h1, phase_train, name='g_bn1'))
        h2 = tf.nn.relu(batch_norm(conv2d(h1, 128, f_h=5,
                            f_w=5, s_h=1, s_w=1, name='g_h2'), phase_train, 'g_bn2'))
        out = tf.nn.tanh(conv2d(h2, 1, f_h=5, f_w=5, s_h=1, s_w=1, name='g_out'))
        return tf.nn.tanh(out)

  #  def sampler(self, images):
  #      tf.get_variable_scope().reuse_variables()
  #      h0 = tf.nn.relu(self.g_bn0(conv2d(images, 32, f_h=40, f_w=1, s_h=1, s_w=1, name='g_h0')))
  #      h1 = tf.nn.relu(self.g_bn1(conv2d(h0, 32, f_h=1, f_w=40, s_h=1, s_w=1, name='g_h1')))
  #      out = tf.nn.tanh(conv2d(h1, 1, f_h=1, f_w=1, s_h=1, s_w=1, name='g_out'))
  #      return out / 2.0 + 0.5

    def init_variables(self, sess, model_file=None):
        """This function returns the number of iterations to add"""
        if model_file is None:
            sess.run(tf.initialize_all_variables())
            return 0
        else:
            self.saver.restore(sess, model_file)
            return int(model_file.split('-')[-1])

    def learn_me(self, num_iters=1001, print_freq=250, model_file=None):
        with tf.Session(config=tf.ConfigProto(gpu_options=self.gpu_options)) as sess:
            log_writer = tf.train.SummaryWriter(
                self.path_to_save + 'logs',
                sess.graph_def,
            )

            to_add = self.init_variables(sess, model_file)
            to_add += print_freq * bool(to_add)

            for iter_counter in xrange(to_add, num_iters + to_add):
                x_batch = self.sharp_gen.next_batch(self.batch_size)
                # TODO: move this inside if
                z_batch, z_true = self.blurred_gen.next_batch(self.batch_size, True)

                if iter_counter % print_freq == 0:
                    self.saver.save(
                        sess, self.path_to_save + 'advnets',
                        iter_counter,
                    )
                    samples, d_cost, g_cost, real_acc, fake_acc = sess.run(
                        [self.samples, self.d_cost, self.g_cost,
                         self.real_accuracy, self.fake_accuracy],
                        feed_dict={self.sharp_imgs: z_true,
                                   self.blurred_imgs: z_batch},
                    )
                    print "Iteration #{}:\n    D cost = {}\
                           \n    G cost = {}\n    Real accuracy = {}\
                           \n    Fake accuracy = {}".format(
                        iter_counter, d_cost, g_cost,
                        real_acc, fake_acc,
                    )
                    sys.stdout.flush()
                    img = self.prettify(samples[:16])
                    imsave(self.path_to_save +
                           'images-{}.png'.format(iter_counter), img)
                    imsave(self.path_to_save + 'images_true-{}.png'.format(iter_counter),
                           self.prettify(z_true[:16]))
                    imsave(self.path_to_save + 'images_blurred-{}.png'.format(iter_counter),
                           self.prettify(z_batch[:16]))

                _, summary = sess.run([self.d_train_step,
                                       self.merged_logs_d], feed_dict={
                    self.sharp_imgs: z_true,
                    self.blurred_imgs: z_batch,
                })
                log_writer.add_summary(summary, iter_counter)

                # TODO: experiment with making this twice?
                # z_batch = self.blurred_gen.next_batch(self.batch_size)
                _, summary = sess.run([self.g_train_step,
                                       self.merged_logs_g], feed_dict={
                    self.blurred_imgs: z_batch,
                    self.sharp_imgs: z_true,
                })
                log_writer.add_summary(summary, iter_counter)

            log_writer.close()

    def gen_images(self, model_file=None, num_imgs=1):
        with tf.Session(config=tf.ConfigProto(gpu_options=self.gpu_options)) as sess:
            self.init_variables(sess, model_file)
            z_batch = self.blurred_gen.next_batch(self.batch_size)
            return sess.run(
                self.samples,
                feed_dict={self.blurred_imgs: z_batch},
            )[:num_imgs]

    def prettify(self, imgs):
        if imgs.shape[-1] == 3:
            return self.prettify3d(imgs)
        else:
            return self.prettify2d(imgs)

    def prettify3d(self, imgs):
        x = imgs.copy()
        sz = x.shape[0]
        while int(np.sqrt(sz)) * int(np.sqrt(sz)) != sz:
            sz += 1
        if sz != x.shape[0]:
            x = np.vstack((x, np.zeros((sz - x.shape[0], x.shape[1]))))
        x = x.reshape(sz, -1)

        sz1 = int(np.sqrt(x.shape[0]))
        sz2 = int(np.sqrt(x.shape[1] / imgs.shape[-1]))
        xr = x.reshape(sz1, sz1, sz2, sz2, imgs.shape[-1])
        sz2 += 1
        xn = np.empty((sz1*sz2, sz1*sz2, imgs.shape[-1]), dtype=np.float)

        for i in range(sz1):
            for j in range(sz1):
                xn[i*sz2:(i + 1)*sz2, j*sz2:(j + 1)*sz2] = \
                    np.lib.pad(xr[i, j], ((1, 0), (1, 0), (0, 0)),
                               'constant', constant_values=(0.5, 0.5))
        return xn[1:, 1:]

    # TODO: count accuracy for the full epoch
    def prettify2d(self, imgs):
        x = np.reshape(imgs.copy(), (imgs.shape[0], -1))
        sz = x.shape[0]
        while int(np.sqrt(sz)) * int(np.sqrt(sz)) != sz:
            sz += 1
        if sz != x.shape[0]:
            x = np.vstack((x, np.zeros((sz - x.shape[0], x.shape[1]))))

        sz1 = int(np.sqrt(x.shape[0]))
        sz2 = int(np.sqrt(x.shape[1]))
        xr = x.reshape(sz1, sz1, sz2, sz2)
        sz2 += 1
        xn = np.empty((sz1*sz2, sz1*sz2), dtype=np.float)

        for i in range(sz1):
            for j in range(sz1):
                xn[i*sz2:(i + 1)*sz2, j*sz2:(j + 1)*sz2] = \
                    np.lib.pad(xr[i, j], ((1, 0), (1, 0)),
                               'constant', constant_values=(0.5, 0.5))
        return xn[1:, 1:]

    def show_images(self, model_file, num_imgs=1):
        img = self.prettify(self.gen_images(model_file, num_imgs))
        plt.axis('off')
        if img.shape[-1] == 3:
            plt.imshow(img)
        else:
            plt.imshow(img, cmap='gray')


if __name__ == '__main__':
    path_to_save = sys.argv[1]
    if len(os.listdir(path_to_save)) != 0:
        print "If you really want to continue learning disable this if.."
        sys.exit(0)
    nets = AdversarialNetworks(save_gpu_memory=False, path_to_save=path_to_save)
    model_file = tf.train.latest_checkpoint(path_to_save)
    nets.learn_me(num_iters=1000001, print_freq=250, model_file=model_file)
