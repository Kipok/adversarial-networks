import tensorflow as tf
import numpy as np
from numpy.lib.stride_tricks import as_strided
import os
from scipy.misc import imsave
from operations import compute_psnr, patchify_images


class BaseModel(object):
    def __init__(self, sess, model_path, dataset,
                 imgs_size=64, num_saved=20):
        """
        This is a base class for all models capable
        of solving image deblurring problem.
        Args:
            sess:       TensorFlow session.
            model_path: path where all the models and debug info is saved.
            imgs_size:  size of the squared image patch this model can deblur.
            num_saved:  number of models that are kept saved during training.
        """
        super(BaseModel, self).__init__()
        self.sess = sess
        self.model_path = model_path
        self.imgs_size = imgs_size
        self.dataset = dataset

        self.init_model()
        self.saver = tf.train.Saver(max_to_keep=num_saved)

    def transform_in(self, imgs):
        """
        Function that is applied to images before fitting them to network.
        You can assume that images are in [0, 255] range.
        """
        return np.expand_dims(imgs, -1) / 127.5 - 1.0

    def transform_out(self, imgs):
        """
        Function that is applied to images produced be the network.
        It has to return images in [0, 255] range.
        """
        return (imgs.squeeze() + 1.0) * 127.5

    def init_model(self):
        """
        This function should create all necessary graph nodes for
        the deblurring model. It has to provide train_net and
        test_net nodes with blur_imgs placeholder holding images.
        """
        raise NotImplementedError("Implement me...")

    def init_model_from_saved(self, init_model_path):
        """
        Function that initializes model weights from another saved model.
        Args:
            init_model_path: path for the saved model.
        Returns:
            ...
        """
        pass

    def train_model(self, load_model_file, output_freq=250,
                    num_iters=50001, batch_size=128):
        """
        Function that runs training of the model.
        Args:
            num_iters:       number of iterations for training.
            batch_size:      batch size for training.
            load_model_file: path to the model file if you want to continue
                             training of None if you're starting from scratch.
        """
        start_iter = 0
        if load_model_file is None:
            self.sess.run(tf.initialize_all_variables())
        else:
            self.saver.restore(self.sess, load_model_file)
            # note that all file names are "advnets-<iternum>"
            start_iter += int(load_model_file.split('-')[-1]) + output_freq

        log_writer = tf.train.SummaryWriter(
            os.path.join(self.model_path, 'logs'),
            self.sess.graph_def,
        )

        for iter_counter in xrange(start_iter, num_iters):
            train_blur, train_real = self.dataset.next_batch(batch_size)
            train_blur = self.transform_in(train_blur)
            train_real = self.transform_in(train_real)
            logs, output_info = self.run_train_step(train_blur, train_real)
            for log in logs:
                log_writer.add_summary(log, iter_counter)
            if iter_counter % output_freq == 0:
                self.saver.save(
                    self.sess,
                    os.path.join(self.model_path, 'models/advnets'),
                    iter_counter,
                )
                output_info = ", ".join("{}: {:.6f}".format(name, val)
                                        for name, val in output_info.items())
                print "Iteration #{}: ".format(iter_counter) + output_info
                print "Testing model..."
                test_blur, test_real = self.dataset.valid_set()
                test_restored = self.deblur_images(test_blur)
                imgs_psnr = compute_psnr(test_real, test_restored)
                print ", ".join("{}: {:.6f}".format(i, val)
                                for i, val in enumerate(imgs_psnr))
                # output some nice test images
                print_dir = os.path.join(self.model_path,
                                         'images/{}'.format(iter_counter))
                os.mkdir(print_dir)
                for i in xrange(test_real.shape[0]):
                    name = os.path.join(print_dir, 'real{}.png'.format(i))
                    imsave(name, test_real[i])
                    name = os.path.join(print_dir, 'blur{}.png'.format(i))
                    imsave(name, test_blur[i])
                    name = os.path.join(print_dir, 'restored{}.png'.format(i))
                    imsave(name, test_restored[i])
        log_writer.close()

    def run_train_step(self, train_blur, train_real):
        """
        This function executes one training iteration.
        Args:
            train_blur: np.array, blur images that serve as input to network.
            train_real: np.array, real images used to compute loss.
        Returns:
            summaries: tf.string containing summaries to write.
            output_info: dict containing all variable for immediate output.
        """
        raise NotImplementedError("Implement me...")

    def deblur_images(self, images, load_model_file=None,
                      step_x=64, step_y=64):
        """
        Function that applies learned model for
        deblurring of the given images. If image is bigger
        than network input size than it will be split in
        patches which will be averaged in overlapping areas.
        Args:
            images:   np.array of images to be deblurred.
            step_x:   integer indicating what stride should be between
                      to patches in x-direction.
            step_y:   integer indicating what stride should be between
                      to patches in y-direction.
        Returns:
            restored: np.array of the same shape as images
                      containing restored images.
        """
        if load_model_file is not None:
            self.saver.restore(self.sess, load_model_file)
        # checking if input images have a correct number of dimensions
        p_size = self.imgs_size
        if images.shape[-1] != 3 and images.shape[-1] != 1:
            images = np.expand_dims(images, -1)
        if len(images.shape) != 4:
            images = np.expand_dims(images, 0)
        if (images.shape[1] - p_size) % step_y != 0:
            name = "step_y has to be a divisor of {} for given parameters"
            raise ValueError(name.format(images.shape[1] - p_size))
        if (images.shape[2] - p_size) % step_x != 0:
            name = "step_x has to be a divisor of {} for given parameters"
            raise ValueError(name.format(images.shape[2] - p_size))
        if step_x > p_size or step_y > p_size:
            raise ValueError("strides have to be less than 64!")
        restored = np.zeros(images.shape, images.dtype)
        averager = np.zeros(images.shape)
        img_patches = patchify_images(images, self.imgs_size, step_x, step_y)
        res_patches = patchify_images(restored, self.imgs_size, step_x, step_y)
        avg_patches = patchify_images(averager, self.imgs_size, step_x, step_y)

        # TODO: surely this can be done in a more convenient way.. :(
        def fill_backwards(imgs, inds, end=-1):
            for ctr, (i, j, k, t, c) in enumerate(inds):
                if ctr == end:
                    break
                res_patches[i, j, k, t, :, :, c] += imgs[ctr]
                avg_patches[i, j, k, t, :, :, c] += 1
        # TODO: this is changeable parameter! Move it somewhere or adjust
        b_size = 100
        idx_shape = img_patches.shape[:4] + (img_patches.shape[-1],)
        indices = np.indices(idx_shape).reshape(5, -1).T
        hold_inds = np.empty((b_size, 5), np.int)
        to_process = np.empty((b_size, p_size, p_size))

        for ctr, (i, j, k, t, c) in enumerate(indices):
            to_process[ctr % b_size] = img_patches[i, j, k, t, :, :, c]
            hold_inds[ctr % b_size] = [i, j, k, t, c]
            if (ctr + 1) % b_size == 0:
                cur_restored = self.transform_out(self.sess.run(
                    self.test_net,
                    {self.blur_imgs: self.transform_in(to_process)},
                ))
                fill_backwards(cur_restored, hold_inds)
        cur_restored = self.transform_out(self.sess.run(
            self.test_net, {self.blur_imgs: self.transform_in(to_process)}
        ))
        fill_backwards(cur_restored, hold_inds, indices.shape[0] % b_size)

        restored = as_strided(res_patches, images.shape, images.strides)
        restored /= as_strided(avg_patches, images.shape, images.strides)
        return restored
