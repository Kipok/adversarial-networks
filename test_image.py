import tensorflow as tf
import numpy as np
from datasets import BlurredDataset, ImagenetDataset
import matplotlib.pyplot as plt
import sys, os
from scipy.misc import imsave, imread

from operations import batch_norm, conv2d, disk
from operations import fully_connected, lrelu


def get_patches(img, ic, jc, size, stride):
    i, j = ic, jc
    n, m = img.shape[:2]
    patches = np.zeros((100, size, size, img.shape[-1]))
    count = 0
    while True:
        if j + size > m:
            j = 0
            i += stride
        if i + size > n or count >= 100:
            break
        patches[count] = img[i:i+size, j:j+size]
        j += stride
        count += 1
    return patches


def restore_channel(image):
    if len(image.shape) == 2:
        img = image[:,:,np.newaxis]
    out_img = np.zeros(img.shape)
    count_img = np.zeros(img.shape)

    n, m = img.shape[:2]
    i, j = 0, 0
    stride, size = 16, 64
    count = 100
    while True:
        if count == 100:
            count = 0
            blurred_patches = get_patches(img, i, j, size, stride)
            patches, mean_, var_ = sess.run([g_out, mean, var],
                                            feed_dict={imgs_b: blurred_patches, g_phase_train: False})
            print mean_

        if j + size > m:
            j = 0
            i += stride
        if i + size > n:
            break
        out_img[i:i+size, j:j+size] += patches[count]
        count_img[i:i+size, j:j+size] += 1
        j += stride
        count += 1
    return out_img / count_img



def generator(images, phase_train):
    h0, mean, var = batch_norm(conv2d(images, 32, f_h=30,
            f_w=1, s_h=1, s_w=1, name='g_h0'), phase_train, name='g_bn0')
    h0 = tf.nn.relu(h0)
    h1, mean, var = batch_norm(conv2d(h0, 32, f_h=1,
            f_w=30, s_h=1, s_w=1, name='g_h1'), phase_train, name='g_bn1')
    h1 = tf.nn.relu(h1)
#    h2 = tf.nn.relu(batch_norm(conv2d(h1, 512, f_h=5,
#            f_w=5, s_h=1, s_w=1, name='g_h2'), phase_train, 'g_bn2'))
    out = tf.nn.tanh(conv2d(h1, 1, f_h=5, f_w=5, s_h=1, s_w=1, name='g_out'))
    return out / 2.0 + 0.5, mean, var

model_path = sys.argv[1]
blurred_path = sys.argv[2]
real_path = sys.argv[3]

save_gpu_memory = False

gpu_memory_fraction = 0.5 if save_gpu_memory else 0.9
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)

imgs_b = tf.placeholder(
    'float',
    [100, 64, 64, 1],
    name='blurred_imgs',
)


g_phase_train = tf.placeholder(tf.bool, name='g_phase_train')
g_out, mean, var = generator(imgs_b, g_phase_train)

saver = tf.train.Saver(max_to_keep=20)

with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
    log_writer = tf.train.SummaryWriter("logs", sess.graph_def)
    saver.restore(sess, os.path.join(model_path, 'advnets-7700'))

    img = imread(blurred_path)
    restored_img = np.empty(img.shape)

    restored_img[:,:,0] = restore_channel(img[:,:,0] / 127.5 - 1.0).squeeze()
    restored_img[:,:,1] = restore_channel(img[:,:,1] / 127.5 - 1.0).squeeze()
    restored_img[:,:,2] = restore_channel(img[:,:,2] / 127.5 - 1.0).squeeze()

#    restored_img[:,:,0] = restore_channel(img[:,:,0] / 127.5 - 1.0).squeeze()
#    restored_img[:,:,1] = restore_channel(img[:,:,1] / 127.5 - 1.0).squeeze()
#    restored_img[:,:,2] = restore_channel(img[:,:,2] / 127.5 - 1.0).squeeze()

real_img = imread(real_path)
imsave(os.path.join(model_path, "restored_img.png"), restored_img)
imsave("images/restored_img.png", restored_img)
print("PSNR = {}".format(psnr(real_img, restored_img * 255.0)))
