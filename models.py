from model_wrapper import BaseModel
import tensorflow as tf
from operations import batch_norm, batch_norm_notest
from operations import lrelu, fully_connected, conv2d


class JoinLossNetwork(BaseModel):
    def __init__(self, supervised, alpha, use_joint, kernel, sess, model_path,
                dataset, run_dnetwork, imgs_size=64, num_saved=20):
        self.alpha = alpha
        self.use_joint = use_joint
        self.kernel = tf.constant(kernel, 'float', [kernel.shape[0], kernel.shape[1], 1, 1])
        self.supervised = supervised
        self.run_dnetwork = run_dnetwork
        super(JoinLossNetwork, self).__init__(sess, model_path, dataset, imgs_size, num_saved)

    def discriminator(self, input_imgs):
        h0 = lrelu(conv2d(input_imgs, 64, name='d_h0'))
        h1 = lrelu(batch_norm_notest(conv2d(h0, 128, name='d_h1'), 'd_bn1'))
        h2 = lrelu(batch_norm_notest(conv2d(h1, 256, name='d_h2'), 'd_bn2'))
        h3 = lrelu(batch_norm_notest(conv2d(h2, 512, name='d_h3'), 'd_bn3'))
        out = tf.nn.sigmoid(fully_connected(h3, 1, name='d_out'))
        return out

 #   def deblur_network(self, input_imgs, is_train):
 #       h0 = tf.nn.relu(batch_norm(conv2d(input_imgs, 32, f_h=30,
 #           f_w=1, s_h=1, s_w=1, name='g_h0'), self.ema, 'g_bn0', is_train))
 #       h1 = tf.nn.relu(batch_norm(conv2d(h0, 32, f_h=1,
 #           f_w=30, s_h=1, s_w=1, name='g_h1'), self.ema, 'g_bn1', is_train))
 #       h2 = tf.nn.relu(batch_norm(conv2d(h1, 128, f_h=5,
 #           f_w=5, s_h=1, s_w=1, name='g_h2'), self.ema, 'g_bn2', is_train))
 #       out = tf.nn.tanh(conv2d(h2, 1, f_h=5, f_w=5, s_h=1, s_w=1, name='g_out'))
 #       return out

    def deblur_network(self, input_imgs, is_train):
        h0 = tf.nn.relu(conv2d(input_imgs, 32, f_h=30,
            f_w=1, s_h=1, s_w=1, name='g_h0'))
        h1 = tf.nn.relu(conv2d(h0, 32, f_h=1,
            f_w=30, s_h=1, s_w=1, name='g_h1'))
        h2 = tf.nn.relu(conv2d(h1, 128, f_h=5,
            f_w=5, s_h=1, s_w=1, name='g_h2'))
        out = tf.nn.tanh(conv2d(h2, 1, f_h=5, f_w=5, s_h=1, s_w=1, name='g_out'))
        return out

    def init_model(self):
        self.blur_imgs = tf.placeholder(
            'float',
            [None, self.imgs_size, self.imgs_size, 1],
            'blurred_imgs',
        )
        self.sharp_imgs = tf.placeholder(
            'float',
            [None, self.imgs_size, self.imgs_size, 1],
            'sharp_imgs',
        )
        self.ema = tf.train.ExponentialMovingAverage(decay=0.5)
        with tf.variable_scope('model'):
            self.train_net = self.deblur_network(self.blur_imgs, True)
        with tf.variable_scope('model', reuse=True):
            self.test_net = self.deblur_network(self.blur_imgs, False)
        with tf.variable_scope('discriminator'):
            d_out = self.discriminator(self.sharp_imgs)
        with tf.variable_scope('discriminator', reuse=True):
            gd_flow_out = self.discriminator(self.train_net)

        t_vars = tf.trainable_variables()
        d_vars = [var for var in t_vars if 'd_' in var.name]
        g_vars = [var for var in t_vars if 'g_' in var.name]

        eps = 1e-12
        self.d_cost = tf.reduce_mean(
            tf.log(1.0 - gd_flow_out + eps) + tf.log(d_out + eps),
            name='d_cost'
        )
        d_cost_sum = tf.scalar_summary('d_cost', self.d_cost)
        self.d_train_step = tf.train.AdamOptimizer(0.00002, 0.5).minimize(
            -self.d_cost, var_list=d_vars,
        )

        self.g_cost = -self.alpha * tf.reduce_mean(tf.log(gd_flow_out + eps), name='g_cost')
        g_cost_sum = tf.scalar_summary('g_cost_reg', self.g_cost)

        if self.supervised:
            self.cost_l2 = tf.reduce_mean(tf.pow(self.sharp_imgs - self.train_net, 2))
        else:
            self.cost_l2 = tf.reduce_mean(tf.pow(tf.nn.conv2d(self.train_net,
                                self.kernel, [1,1,1,1], 'SAME') - self.blur_imgs, 2))
        cost_l2_sum = tf.scalar_summary('cost_l2', self.cost_l2)

        if self.use_joint:
            self.total_cost = self.g_cost + self.cost_l2
        else:
            self.total_cost = self.cost_l2
        total_cost_sum = tf.scalar_summary('total_cost', self.total_cost)

        self.train_step = tf.train.AdamOptimizer(0.001, 0.9).minimize(
            self.total_cost, var_list=g_vars,
        )

        self.real_accuracy = tf.reduce_mean(tf.cast(
            tf.equal(tf.round(d_out), 1),
            'float',
        ), name='real_accuracy')
        racc_sum = tf.scalar_summary('real_accuracy', self.real_accuracy)

        self.fake_accuracy = tf.reduce_mean(tf.cast(
            tf.equal(tf.round(gd_flow_out), 0),
            'float',
        ), name='fake_accuracy')
        facc_sum = tf.scalar_summary('fake_accuracy', self.fake_accuracy)

        self.merged_logs_d = tf.merge_summary([d_cost_sum, racc_sum, facc_sum])
        self.merged_logs_g = tf.merge_summary([total_cost_sum, cost_l2_sum, g_cost_sum])

 #   def run_train_step(self, train_blur, train_real):
 #       b_size = 20
 #       cur = 0
 #       # update discriminator
 #       if self.run_dnetwork:
 #           for i in range(10):
 #               _, real_accuracy, fake_accuracy, d_cost, summ_d = self.sess.run(
 #                   [self.d_train_step, self.real_accuracy, self.fake_accuracy,
 #                    self.d_cost, self.merged_logs_d],
 #                   {self.blur_imgs: train_blur[cur:cur+b_size],
 #                    self.sharp_imgs: train_real[cur:cur+b_size]},
 #               )
 #               cur += b_size
 #       # update model
 #       for i in range(40):
 #           _, cost_l2, g_cost, total_cost, summ_g = self.sess.run(
 #               [self.train_step, self.cost_l2, self.g_cost, self.total_cost, self.merged_logs_g],
 #               {self.blur_imgs: train_blur[cur:cur+b_size],
 #                self.sharp_imgs: train_real[cur:cur+b_size]},
 #           )
 #           cur += b_size
 #       if self.run_dnetwork:
 #           return [summ_d, summ_g], \
 #               {'real_accuracy': real_accuracy, 'fake_accuracy': fake_accuracy,
 #               'd_cost': d_cost, 'g_cost': g_cost, 'l2_cost': cost_l2, 'total_cost': total_cost}
 #       else:
 #           return [summ_g], {'l2_cost': cost_l2}

    def run_train_step(self, train_blur, train_real):
        # update discriminator
        if self.run_dnetwork:
            _, real_accuracy, fake_accuracy, d_cost, summ_d = self.sess.run(
                [self.d_train_step, self.real_accuracy, self.fake_accuracy,
                 self.d_cost, self.merged_logs_d],
                {self.blur_imgs: train_blur,
                 self.sharp_imgs: train_real},
            )
        # update model
        _, cost_l2, g_cost, total_cost, summ_g = self.sess.run(
            [self.train_step, self.cost_l2, self.g_cost, self.total_cost, self.merged_logs_g],
            {self.blur_imgs: train_blur,
             self.sharp_imgs: train_real},
        )
        if self.run_dnetwork:
            return [summ_d, summ_g], \
                {'real_accuracy': real_accuracy, 'fake_accuracy': fake_accuracy,
                'd_cost': d_cost, 'g_cost': g_cost, 'l2_cost': cost_l2, 'total_cost': total_cost}
        else:
            return [summ_g], {'l2_cost': cost_l2}

class SimpleNetwork(BaseModel):
    # TODO: rewrite it using args and kwargs
    def __init__(self, sess, model_path, dataset, imgs_size=64, num_saved=20):
        super(SimpleNetwork, self).__init__(sess, model_path, dataset, imgs_size, num_saved)

    def deblur_network(self, input_imgs, is_train):
        h0 = tf.nn.relu(batch_norm(conv2d(input_imgs, 32, f_h=30,
            f_w=1, s_h=1, s_w=1, name='g_h0'), self.ema, 'g_bn0', is_train))
        h1 = tf.nn.relu(batch_norm(conv2d(h0, 32, f_h=1,
            f_w=30, s_h=1, s_w=1, name='g_h1'), self.ema, 'g_bn1', is_train))
        h2 = tf.nn.relu(batch_norm(conv2d(h1, 128, f_h=5,
            f_w=5, s_h=1, s_w=1, name='g_h2'), self.ema, 'g_bn2', is_train))
        out = tf.nn.tanh(conv2d(h2, 1, f_h=5, f_w=5, s_h=1, s_w=1, name='g_out'))
        return out

    def init_model(self):
        self.blur_imgs = tf.placeholder(
            'float',
            [None, self.imgs_size, self.imgs_size, 1],
            'blurred_imgs',
        )
        self.sharp_imgs = tf.placeholder(
            'float',
            [None, self.imgs_size, self.imgs_size, 1],
            'sharp_imgs',
        )
        self.ema = tf.train.ExponentialMovingAverage(decay=0.5)
        with tf.variable_scope('model'):
            self.train_net = self.deblur_network(self.blur_imgs, True)
        with tf.variable_scope('model', reuse=True):
            self.test_net = self.deblur_network(self.blur_imgs, False)

        self.cost = tf.reduce_mean(tf.pow(self.sharp_imgs - self.train_net, 2))
        self.summary = tf.scalar_summary('cost', self.cost)
        self.train_step = tf.train.AdamOptimizer(0.0002).minimize(self.cost)


    def run_train_step(self, train_blur, train_real):
        _, cost_v, summary = self.sess.run([self.train_step, self.cost, self.summary],
                        feed_dict={self.blur_imgs: train_blur,
                                   self.sharp_imgs: train_real})
        return [summary], {'cost': cost_v}
