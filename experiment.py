from models import JoinLossNetwork
import numpy as np
import tensorflow as tf
from operations import disk
from datasets import ImagenetDataset
# from datasets import ImagenetSmall
import sys
import os
import shutil


if __name__ == '__main__':
    model_path = sys.argv[1]
    gpu_memory = float(sys.argv[2])

    if len(os.listdir(model_path)) != 0:
        if len(sys.argv) > 3:
            if sys.argv[3] == '--discard':
                shutil.rmtree(model_path)
                os.mkdir(model_path)
                os.mkdir(os.path.join(model_path, 'images'))
                os.mkdir(os.path.join(model_path, 'models'))
                print "Discarding model directory and learning from scratch"

            elif sys.argv[3] == '--continue':
                print "Continuing learning from the last checkpoint"
            else:
                print "Folder exists, provide --discard or --continue option"
                sys.exit(0)
        else:
            print "Folder exists, provide --discard or --continue option"
            sys.exit(0)

    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory)
    kernel = disk(15)
    size = 64
    noise = np.sqrt(0.0005)
    saturation = 1.3
    alpha = 0.001
    use_joint = False
    supervised = True
    run_dnetwork = False
    dataset = ImagenetDataset(size, noise, kernel, saturation)

    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
        model = JoinLossNetwork(supervised, alpha, use_joint, kernel,
                                sess, model_path, dataset, run_dnetwork)
        model.train_model(
            tf.train.latest_checkpoint(os.path.join(model_path, 'models')),
            batch_size=128,
            output_freq=250,
        )
