commit_name="` git rev-parse --abbrev-ref HEAD `__` git log -1 --pretty=%B | tr ' ' '_' `"
printf "\033]2;%s\033\\" "$commit_name"
dirname=/srv/hd2/data/advnets_experiments/"$commit_name/"
mkdir "$dirname"
mkdir "$dirname/images"
mkdir "$dirname/models"
echo CUDA_VISIBLE_DEVICES=$1 python experiment.py $dirname $2 $3
CUDA_VISIBLE_DEVICES=$1 python experiment.py $dirname $2 $3
