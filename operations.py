import tensorflow as tf
import numpy as np
from numpy.lib.stride_tricks import as_strided
from scipy import ndimage


def patchify_images(imgs, patch_size, step_x, step_y):
    """
    This is a nice function that uses strides magic to reshape
    input array into patches taken with step_x stride in x-direction
    step_y stride in y-direction and patch_size size in both directions.
    It's important to note that this function doesn't make the copy of
    the input array and thus there can be arbitraty number of patches.
    Args:
        imgs:       np.array of input images
        patch_size: integer size of the patch
        step_x:     integer stride in x-direction
        step_y:     integer stride in y-direction
    Returns:
        patches: np.array of shape (num-imgs, num-y-patches, num-x-patches,
                                    1, patch_size, patch_size, imgs.shape[-1])
    """
    img_size = imgs.shape[1:]
    assert((img_size[0] - patch_size) % step_y == 0)
    assert((img_size[1] - patch_size) % step_x == 0)

    patch_shape = (patch_size, patch_size, imgs.shape[-1])
    patch_stride = np.array((1, step_y, step_x, 1))

    tmp = (np.array(imgs.shape) - np.array((1,)
           + patch_shape)) // patch_stride + 1
    shape = tuple(list(tmp) + list(patch_shape))
    strides = (imgs.strides[0], imgs.strides[1] * patch_stride[1],
               imgs.strides[2] * patch_stride[2],
               imgs.strides[3], imgs.strides[1],
               imgs.strides[2], imgs.strides[3])

    patches = as_strided(imgs, shape=shape, strides=strides)
    return patches


def psnr(img1, img2):
    return 10 * np.log10(255 ** 2 / np.mean((img1 - img2) ** 2))


def compute_psnr(imgs1, imgs2):
    """
    This function computes psnr for each pair
    of original and restored images.
    Args:
        imgs1:     np.array containing clean images
        imgs2:     np.array containing restored images
    Returns:
        imgs_psnr: np.array containing psnr values for each pair
    """
    imgs_psnr = np.empty(imgs1.shape[0])
    for i in xrange(imgs1.shape[0]):
        imgs_psnr[i] = psnr(imgs1[i], imgs2[i])
    return imgs_psnr


def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.114])


def crop2range(img):
    img[img < 0] = 0
    img[img > 255] = 255
    return np.round(img)


def blur_img(img, kernel):
    bimg = np.empty(img.shape)
    if len(img.shape) == 2:
        return ndimage.convolve(img, kernel)
    bimg[:, :, 0] = ndimage.convolve(img[:, :, 0], kernel)
    bimg[:, :, 1] = ndimage.convolve(img[:, :, 1], kernel)
    bimg[:, :, 2] = ndimage.convolve(img[:, :, 2], kernel)
    return bimg


def noise_imgs(img, sigma):
    """ Assumes image to be in range [0, 255] """
    img += sigma * 255 * np.random.standard_normal(img.shape)
    img[img < 0] = 0
    img[img > 255] = 255
    return img.round()


def center_crop(x, crop_sz):
    i = int(round((x.shape[0] - crop_sz) / 2.0))
    j = int(round((x.shape[1] - crop_sz) / 2.0))
    return x[i:i+crop_sz, j:j+crop_sz]


def random_crop(x, crop_sz):
    i = np.random.choice(x.shape[0] - crop_sz, 1)
    j = np.random.choice(x.shape[1] - crop_sz, 1)
    return x[i:i+crop_sz, j:j+crop_sz], i, j


def crop(x, crop_sz, i, j):
    return x[i:i+crop_sz, j:j+crop_sz]


def disk(n):
    a, b = n // 2, n // 2
    r = n // 2
    y, x = np.ogrid[-a:n-a, -b:n-b]
    mask = x*x + y*y <= r*r
    return mask.astype(np.float) / np.sum(mask)


def lrelu(x, leak=0.2, name="lrelu"):
    """Leaky ReLU activation function which is max(x, leak * x)"""
    with tf.variable_scope(name):
        f1 = 0.5 * (1 + leak)
        f2 = 0.5 * (1 - leak)
        return f1 * x + f2 * abs(x)


#class batch_norm(object):
#    def __init__(self, name):
#        """
#        Batch normalization on convolutional maps.
#        Args:
#            x:        Tensor, 4D BHWD input maps
#            is_train: boolean, true indicates training phase
#            name:     string, variable scope
#        Return:
#            normed:   batch-normalized maps
#        """
#        self.ema = tf.train.ExponentialMovingAverage(decay=0.5)
#        self.name = name
#
#    def __call__(x, is_train):
#        n_out = x.get_shape().as_list()[-1]
#        with tf.variable_scope(name):
#            beta = tf.get_variable('beta', [n_out],
#                initializer=tf.constant_initializer(0.0))
#            gamma = tf.get_variable('gamma', [n_out],
#                initializer=tf.constant_initializer(1.0))
#            batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')


def batch_norm_notest(x, name, eps=1e-5):
    n_out = x.get_shape().as_list()[-1]
    with tf.variable_scope(name):
        beta = tf.get_variable('beta', [n_out], initializer=tf.constant_initializer(0.0))
        gamma = tf.get_variable('gamma', [n_out], initializer=tf.constant_initializer(1.0))
        mean, var = tf.nn.moments(x, [0, 1, 2])
        return tf.nn.batch_norm_with_global_normalization(
            x, mean, var, beta, gamma, eps, True)


def batch_norm(x, ema, name, train=True, eps=1e-5):
    n_out = x.get_shape().as_list()[-1]
    with tf.variable_scope(name):
        total_mean = tf.get_variable(
            'mean', [n_out],
            initializer=tf.constant_initializer(0.0),
            trainable=False,
        )
        total_var = tf.get_variable(
            'var', [n_out],
            initializer=tf.constant_initializer(1.0),
            trainable=False
        )
        beta = tf.get_variable('beta', [n_out], initializer=tf.constant_initializer(0.0))
        gamma = tf.get_variable('gamma', [n_out], initializer=tf.constant_initializer(1.0))
        if train:
            mean, var = tf.nn.moments(x, [0, 1, 2])
            with tf.control_dependencies([total_mean.assign(mean), total_var.assign(var)]):
                with tf.control_dependencies([ema.apply([total_mean, total_var])]):
                    mean, var = tf.identity(mean), tf.identity(var)
            return tf.nn.batch_norm_with_global_normalization(
                x, mean, var, beta, gamma,
                eps, True)
        else:
            mean = ema.average(total_mean)
            var = ema.average(total_var)
            return tf.nn.batch_norm_with_global_normalization(
                x, mean, var, tf.identity(beta), tf.identity(gamma),
                eps, True)


#def batch_norm(x, is_train, name, ema, affine=True, tmp=False):
#    """
#    Batch normalization on convolutional maps.
#    Args:
#        x:        Tensor, 4D BHWD input maps
#        is_train: boolean, true indicates training phase
#        name:     string, variable scope
#        affine:   whether to affine-transform outputs
#    Return:
#        normed:   batch-normalized maps
#    """
#    n_out = x.get_shape().as_list()[-1]
#    with tf.variable_scope(name):
#        beta = tf.get_variable('beta', [n_out],
#            initializer=tf.constant_initializer(0.0), trainable=True)
#        gamma = tf.get_variable('gamma', [n_out],
#            initializer=tf.constant_initializer(1.0), trainable=affine)
#
#        batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')
#        ema_apply_op = ema.apply([batch_mean, batch_var])
#        ema_mean, ema_var = ema.average(batch_mean), ema.average(batch_var)
#
#        def mean_var_with_update():
#            with tf.control_dependencies([ema_apply_op]):
#                return tf.identity(batch_mean), tf.identity(batch_var)
#        if is_train:
#            mean, var = mean_var_with_update()
#        else:
#            mean, var = ema_mean, ema_var
#        normed = tf.nn.batch_norm_with_global_normalization(x, mean, var,
#            beta, gamma, 1e-5, affine)
#    if tmp:
#        return normed, mean, var
#    return normed
#    def __init__(self, batch_size, epsilon=1e-5,
#                 momentum=0.1, name="batch_norm"):
#        with tf.variable_scope(name):
#            self.epsilon = epsilon
#            self.momentum = momentum
#            self.batch_size = batch_size
#
#            self.ema = tf.train.ExponentialMovingAverage(decay=self.momentum)
#            self.name = name
#        # TODO: FIX THIS train STUFFFFFF!!!!!!!!
#
#    def __call__(self, x, train=True):
#        shape = x.get_shape().as_list()
#
#        with tf.variable_scope(self.name):
#            self.gamma = tf.get_variable("gamma", [shape[-1]],
#                             initializer=tf.random_normal_initializer(1.0, 0.02))
#            self.beta = tf.get_variable("beta", [shape[-1]],
#                            initializer=tf.constant_initializer(0.0))
#
#            self.mean, self.variance = tf.nn.moments(x, [0, 1, 2])
#
#            return tf.nn.batch_norm_with_global_normalization(
#                x, self.mean, self.variance, self.beta,
#                self.gamma, self.epsilon,
#                scale_after_normalization=True)


def conv2d(input_l, out_size, f_h=5, f_w=5, s_h=2, s_w=2,
           pad="SAME", stddev=0.02, name="conv2d", with_w=False):
    # TODO: add log summary?
    with tf.variable_scope(name):
        # TODO: truncated normal change to usual normal?
        w = tf.get_variable('w', [f_h, f_w, input_l.get_shape()[-1], out_size],
                    initializer=tf.truncated_normal_initializer(stddev=stddev))
        b = tf.get_variable('b', [out_size], initializer=tf.constant_initializer(0.0))
        conv = tf.nn.conv2d(input_l, w, strides=[1, s_h, s_w, 1], padding=pad) + b
        if with_w is True:
            return conv, w, b
        return conv


def deconv2d(input_l, output_shape, f_h=5, f_w=5, s_h=2, s_w=2,
             pad="SAME", stddev=0.02, name="deconv2d", with_w=False):
    with tf.variable_scope(name):
        w = tf.get_variable('w', [f_h, f_w, output_shape[-1], input_l.get_shape()[-1]],
                            initializer=tf.random_normal_initializer(stddev=stddev))
        b = tf.get_variable('b', [output_shape[-1]],
                            initializer=tf.constant_initializer(0.0))
        deconv = tf.nn.deconv2d(input_l, w, output_shape=output_shape,
                                strides=[1, s_h, s_w, 1], padding=pad) + b
        if with_w is True:
            return deconv, w, b
        return deconv


def fully_connected(input_l, out_size, stddev=0.02, name="fully", with_w=False):
    with tf.variable_scope(name):
        shape = input_l.get_shape().as_list()
        flattened = tf.reshape(input_l, [-1, shape[1]*shape[2]*shape[3]])
        w = tf.get_variable('w', [flattened.get_shape()[1], out_size],
                            initializer=tf.random_normal_initializer(stddev=stddev))
        b = tf.get_variable('b', [out_size], initializer=tf.constant_initializer(0.0))
        if with_w is True:
            return tf.matmul(flattened, w) + b, w, b
        return tf.matmul(flattened, w) + b
