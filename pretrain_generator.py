import tensorflow as tf
import numpy as np
from datasets import BlurredDataset, ImagenetDataset
import matplotlib.pyplot as plt
import sys, os
from scipy.misc import imsave

from operations import batch_norm, conv2d, disk
from operations import fully_connected, lrelu

def prettify( imgs):
    if imgs.shape[-1] == 3:
        return prettify3d(imgs)
    else:
        return prettify2d(imgs)

def prettify3d( imgs):
    x = imgs.copy()
    sz = x.shape[0]
    while int(np.sqrt(sz)) * int(np.sqrt(sz)) != sz:
        sz += 1
    if sz != x.shape[0]:
        x = np.vstack((x, np.zeros((sz - x.shape[0], x.shape[1]))))
    x = x.reshape(sz, -1)

    sz1 = int(np.sqrt(x.shape[0]))
    sz2 = int(np.sqrt(x.shape[1] / imgs.shape[-1]))
    xr = x.reshape(sz1, sz1, sz2, sz2, imgs.shape[-1])
    sz2 += 1
    xn = np.empty((sz1*sz2, sz1*sz2, imgs.shape[-1]), dtype=np.float)

    for i in range(sz1):
        for j in range(sz1):
            xn[i*sz2:(i + 1)*sz2, j*sz2:(j + 1)*sz2] = \
                np.lib.pad(xr[i, j], ((1, 0), (1, 0), (0, 0)),
                           'constant', constant_values=(0.5, 0.5))
    return xn[1:, 1:]

# TODO: count accuracy for the full epoch
def prettify2d( imgs):
    x = np.reshape(imgs.copy(), (imgs.shape[0], -1))
    sz = x.shape[0]
    while int(np.sqrt(sz)) * int(np.sqrt(sz)) != sz:
        sz += 1
    if sz != x.shape[0]:
        x = np.vstack((x, np.zeros((sz - x.shape[0], x.shape[1]))))

    sz1 = int(np.sqrt(x.shape[0]))
    sz2 = int(np.sqrt(x.shape[1]))
    xr = x.reshape(sz1, sz1, sz2, sz2)
    sz2 += 1
    xn = np.empty((sz1*sz2, sz1*sz2), dtype=np.float)

    for i in range(sz1):
        for j in range(sz1):
            xn[i*sz2:(i + 1)*sz2, j*sz2:(j + 1)*sz2] = \
                np.lib.pad(xr[i, j], ((1, 0), (1, 0)),
                           'constant', constant_values=(0.5, 0.5))
    return xn[1:, 1:]

def generator(images, phase_train):
    h0 = tf.nn.relu(batch_norm(conv2d(images, 32, f_h=30,
            f_w=1, s_h=1, s_w=1, name='g_h0'), phase_train, 'g_bn0'))
    h1 = tf.nn.relu(batch_norm(conv2d(h0, 32, f_h=1,
            f_w=30, s_h=1, s_w=1, name='g_h1'), phase_train, 'g_bn1'))
    h2 = tf.nn.relu(batch_norm(conv2d(h1, 128, f_h=5,
            f_w=5, s_h=1, s_w=1, name='g_h2'), phase_train, 'g_bn2'))
    out = tf.nn.tanh(conv2d(h2, 1, f_h=5, f_w=5, s_h=1, s_w=1, name='g_out'))
    return out


path_to_save = sys.argv[1]
#if len(os.listdir(path_to_save)) != 0:
#    print "If you really want to continue learning disable this if.."
#    sys.exit(0)

save_gpu_memory = True

gpu_memory_fraction = 0.5 if save_gpu_memory else 0.9
gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)

g_phase_train = tf.placeholder(tf.bool, name='g_phase_train')

imgs_b = tf.placeholder(
    'float',
    [100, 64, 64, 1],
    name='blurred_imgs',
)

imgs_s = tf.placeholder(
    'float',
    [100, 64, 64, 1],
    name='sharp_images'
)

kernel = tf.constant(disk(7), 'float', [7, 7, 1, 1])

g_out = generator(imgs_b, g_phase_train)

t_vars = tf.trainable_variables()
g_vars = [var for var in t_vars if 'g_' in var.name]

cost = tf.reduce_sum(tf.pow(imgs_s - g_out, 2))
train_step = tf.train.AdamOptimizer(0.0002).minimize(cost, var_list=g_vars)

saver = tf.train.Saver(max_to_keep=20)
data = BlurredDataset()

with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
    #sess.run(tf.initialize_all_variables())
    saver.restore(sess, tf.train.latest_checkpoint(path_to_save))

    for i in xrange(8000, 100000):
        batch_b, batch_s = data.next_batch(100, True)
        _, cost_v, samples = sess.run([train_step, cost, g_out],
                        feed_dict={imgs_b: batch_b, imgs_s: batch_s, g_phase_train: True})
        if i % 100 == 0:
            print "Iteration #{}: cost #{}".format(i, cost_v)
            saver.save(sess, path_to_save + 'advnets', i)
            img = prettify(samples[:16])
            imsave(path_to_save + 'images-{}.png'.format(i), img / 2.0 + 0.5)
            imsave(path_to_save + 'images_true-{}.png'.format(i), prettify(batch_s[:16]) / 2.0 + 0.5)
            imsave(path_to_save + 'images_blurred-{}.png'.format(i), prettify(batch_b[:16]) / 2.0 + 0.5)
