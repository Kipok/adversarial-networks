import numpy as np
from scipy.misc import imread
from operations import random_crop, crop, blur_img, noise_imgs, crop2range
import os


class BlurDataset(object):
    def __init__(self, size, sigma, kernel, saturation):
        """
        Base class for all datasets. Handles reading and transforming
        images. Also this class should have methods for artificial blur.
        Note that all images are converted to grayscale.
        Args:
            size:   integer, size of the image crops.
            sigma:  float, sigma for gaussian noise.
            kernel: np.array, blur kernel to apply.
        """
        self.epoch_counter = 0
        self.cur = 0
        self.size = size
        self.sigma = sigma
        self.kernel = kernel
        self.saturation = saturation

    def next_batch(self, b_size):
        if self.cur + b_size > len(self.img_names):
            self.cur = 0
            np.random.shuffle(self.img_names)
            self.epoch_counter += 1
            print "Starting epoch #{}".format(self.epoch_counter)
        batch = self.read_imgs(b_size)
        self.cur += b_size
        return batch

    def read_imgs(self, b_size):
        if not hasattr(self, 'img_names'):
            raise NotImplementedError('"img_names" variable doesn\'t exist')
        real_imgs = np.empty((b_size, self.size, self.size))
        blur_imgs = np.empty((b_size, self.size, self.size))
        for i in range(self.cur, self.cur + b_size):
            # reading image as grayscale
            img = imread(self.img_names[i], 1)
            idx = i - self.cur
            real_imgs[idx], ic, jc = random_crop(img, self.size)
            blur_imgs[idx] = crop(self.degrade(img), self.size, ic, jc)
        return blur_imgs, crop2range(real_imgs * self.saturation)

    # TODO: add jpeg
    def degrade(self, imgs):
        if len(imgs.shape) == 2 or len(imgs.shape) == 3 and imgs.shape[-1] == 3:
            return crop2range(noise_imgs(blur_img(imgs * self.saturation, self.kernel), self.sigma))
        else:
            blurred = np.array(map(lambda x: blur_img(x * self.saturation, self.kernel), imgs))
            return crop2range(noise_imgs(blurred, self.sigma))

    def valid_set(self):
        if not hasattr(self, 'valid_imgs'):
            raise NotImplementedError('"valid_imgs" variable doesn\'t exist')
        return self.degrade(self.valid_imgs), crop2range(self.valid_imgs * self.saturation)


class ImagenetDataset(BlurDataset):
    def __init__(self, size, sigma, kernel, saturation):
        super(ImagenetDataset, self).__init__(size, sigma, kernel, saturation)
        d_name = '/srv/hd1/data/imagenet12-256x256/images/train'
        self.img_names = [os.path.join(d_name, subdir, name)
                          for subdir in os.listdir(d_name)
                          for name in os.listdir(os.path.join(d_name, subdir))]
        valid_dir = '/srv/hd2/data/valid_set'
        self.valid_imgs = np.array([imread(os.path.join(valid_dir, name))
                                   for name in os.listdir(valid_dir)])
        np.random.shuffle(self.img_names)

class ImagenetSmall(BlurDataset):
    def __init__(self, size, sigma, kernel, saturation):
        super(ImagenetSmall, self).__init__(size, sigma, kernel, saturation)
        valid_dir = '/srv/hd2/data/valid_set'
        self.valid_imgs = np.array([imread(os.path.join(valid_dir, name))
                                   for name in os.listdir(valid_dir)])
        self.img_names = [os.path.join(valid_dir, name) for name in os.listdir(valid_dir)]
